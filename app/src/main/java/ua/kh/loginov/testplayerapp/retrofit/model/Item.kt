package ua.kh.loginov.testplayerapp.retrofit.model

import com.google.gson.annotations.SerializedName

data class Item (
    @SerializedName("id") val id : String,
    @SerializedName("snippet") val snippet : Snippet
)