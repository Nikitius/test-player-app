package ua.kh.loginov.testplayerapp.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import ua.kh.loginov.testplayerapp.MyApplication

abstract class BaseActivity<Binding : ViewDataBinding, ViewModel : ActivityViewModel> : AppCompatActivity() {

    protected lateinit var binding: Binding
    protected lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind()
    }

    private fun bind() {
        binding = DataBindingUtil.setContentView(this, getLayout())
        viewModel = createViewModel()
        binding.setVariable(getVariable(), viewModel)
        binding.executePendingBindings()
    }

    fun getMyApplication(): MyApplication {
        return application as MyApplication
    }

    protected abstract fun getLayout(): Int
    protected abstract fun createViewModel(): ViewModel
    protected abstract fun getVariable(): Int

}