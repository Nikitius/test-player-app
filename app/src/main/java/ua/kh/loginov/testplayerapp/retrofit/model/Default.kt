package ua.kh.loginov.testplayerapp.retrofit.model

import com.google.gson.annotations.SerializedName

data class Default (
    @SerializedName("url") val url : String
)