package ua.kh.loginov.testplayerapp.ui.activities.player

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.MergingMediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import ua.kh.loginov.testplayerapp.base.ActivityViewModel
import ua.kh.loginov.testplayerapp.model.VideoItem


class PlayerActivityViewModel(
    callback: AppCompatActivity,
    videos: List<VideoItem>,
    var currentPosition: Int
) : ActivityViewModel(callback) {

    companion object {
        private const val INSTANCE_STATE_POSITION = "position"
        private const val INSTANCE_STATE_TIME_MILLIS = "timeMillis"
    }

    val player: SimpleExoPlayer = SimpleExoPlayer.Builder(callback).build()

    init {
        val mms = ArrayList<MergingMediaSource>()

        val ddsf = DefaultDataSourceFactory(callback)
        for (item in videos)
            mms.add(MergingMediaSource(
                ProgressiveMediaSource.Factory(ddsf).createMediaSource(Uri.parse(item.videoURI)),
                ProgressiveMediaSource.Factory(ddsf).createMediaSource(Uri.parse(item.audioURI))
            ))

        player.setMediaSources(mms as List<MediaSource>)

        player.prepare()
        if (currentPosition != 0)
            for (i in 0 until currentPosition)
                player.next()

        player.addListener(object : Player.EventListener {
            override fun onMediaItemTransition(mediaItem: MediaItem?, reason: Int) {
                super.onMediaItemTransition(mediaItem, reason)
                currentPosition = player.currentWindowIndex
            }
        })
    }

    fun onRestoreInstanceState(savedInstanceState: Bundle) {
        currentPosition = savedInstanceState.getInt(INSTANCE_STATE_POSITION)
        while (player.currentWindowIndex < currentPosition)
            player.next()
        while (player.currentWindowIndex > currentPosition)
            player.previous()

        player.seekTo(savedInstanceState.getLong(INSTANCE_STATE_TIME_MILLIS))
    }

    fun onPause() {
        player.pause()
    }

    fun onSaveInstanceState(): Bundle {
        val outState = Bundle()
        outState.putInt(INSTANCE_STATE_POSITION, currentPosition)
        outState.putLong(INSTANCE_STATE_TIME_MILLIS, player.currentPosition)
        return outState
    }
}