package ua.kh.loginov.testplayerapp.data

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ua.kh.loginov.testplayerapp.model.VideoItem
import ua.kh.loginov.testplayerapp.model.VideoItemDao
import ua.kh.loginov.testplayerapp.retrofit.MyRetrofit
import ua.kh.loginov.testplayerapp.retrofit.model.Root

class DataProvider(
    private val myRetrofit: MyRetrofit,
    private val videoItemDao: VideoItemDao,
    private val observer: Observer<List<VideoItem>>
) {

    fun provide() {
        object : Thread() {
            override fun run() {
                super.run()
                if (videoItemDao.getAmountForStatus(VideoItem.Status.CACHED) > 0)
                    provideFromDB()
                else
                    provideFromCombined()
            }
        }.start()
    }

    private fun provideFromDB() {
        Observable
            .just(videoItemDao.getAll())
            .subscribe(observer)
    }

    private fun provideFromCombined() {
        myRetrofit
            .getYouTubeAPI()
            .getTopTenPopularVideos()
            .enqueue(object : Callback<Root> {

                override fun onFailure(call: Call<Root>, t: Throwable) {
                    throw t
                }

                override fun onResponse(
                    call: Call<Root>,
                    response: Response<Root>
                ) {
                    object : Thread() {
                        override fun run() {
                            super.run()
                            val videoItems = DataUtils.retrofitModelToAppModel(response.body() as Root, 3)
                            val stored = videoItemDao.getAll()
                            for (videoItem in videoItems)
                                if (!stored.contains(videoItem))
                                    videoItemDao.insert(videoItem)
                            Observable
                                .just(videoItemDao.getAll())
                                .subscribe(observer)
                        }
                    }.start()
                }

            })
    }

}