package ua.kh.loginov.testplayerapp.dagger.modules

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ua.kh.loginov.testplayerapp.model.MyDatabase
import ua.kh.loginov.testplayerapp.model.VideoItemDao
import javax.inject.Singleton

@Module
class MyDatabaseModule(private val context: Context) {

    companion object {
        private const val DATABASE_NAME = "videos_db"
    }

    private fun provideMyDatabase(): MyDatabase {
        return Room.databaseBuilder(
            context,
            MyDatabase::class.java,
            DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideVideoItemDao(): VideoItemDao {
        return provideMyDatabase().videoItemDao()
    }
}