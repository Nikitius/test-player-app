package ua.kh.loginov.testplayerapp.retrofit


import retrofit2.Call
import retrofit2.http.GET
import ua.kh.loginov.testplayerapp.retrofit.model.Root

interface YouTubeAPI {
    @GET("/youtube/v3/videos?part=snippet&chart=mostPopular&maxResults=15&key=AIzaSyBmjpmygpzl0vAv02yn3WdagGClaWBvt-E")
    fun getTopTenPopularVideos(): Call<Root>
}