package ua.kh.loginov.testplayerapp.ui

import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.squareup.picasso.Picasso
import ua.kh.loginov.testplayerapp.R
import ua.kh.loginov.testplayerapp.base.OnChangeListener
import ua.kh.loginov.testplayerapp.base.OnClickListener
import ua.kh.loginov.testplayerapp.model.VideoItem
import ua.kh.loginov.testplayerapp.ui.adapters.VideoAdapter

@BindingAdapter("app:adapter")
fun onBindAdapter(rv: RecyclerView, va: VideoAdapter){
    rv.adapter = va
}

@BindingAdapter("app:image")
fun onBindImage(iv: ImageView, vi: VideoItem) {
    if (vi.status == VideoItem.Status.DOWNLOADED)
        iv.setImageURI(Uri.parse(vi.imageURL))
    else
        Picasso.with(iv.context).load(vi.imageURL).into(iv)
}

@BindingAdapter("app:src")
fun onBindDownloadedIcon(iv: ImageView, s: VideoItem.Status) {
    when (s) {
        VideoItem.Status.DOWNLOADED -> {
            iv.setImageResource(R.drawable.ic_downloaded_24)
            iv.isClickable = false
        }
        VideoItem.Status.DOWNLOADING -> {
            iv.setImageResource(R.drawable.ic_downloading_24)
            iv.isClickable = false
        }
        else -> {
            iv.setImageResource(R.drawable.ic_download_24)
            iv.isClickable = true
        }
    }
}

@BindingAdapter("app:onClick")
fun onBindOnClickListener(v: View, ocl: OnClickListener) {
    v.setOnClickListener(ocl::onClick)
}

@BindingAdapter("app:player")
fun onBindPlayer(spv: StyledPlayerView, sep: SimpleExoPlayer) {
    spv.player = sep
}

@BindingAdapter("app:visibility")
fun onBindItems(pb: ProgressBar, ocl: OnChangeListener<ProgressBar?>) {
    ocl.`object` = pb
    ocl.onChange()
}