package ua.kh.loginov.testplayerapp.retrofit.model

import com.google.gson.annotations.SerializedName

data class Thumbnails (
    @SerializedName("default") val default : Default
)