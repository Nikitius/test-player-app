package ua.kh.loginov.testplayerapp.dagger.components

import dagger.Component
import ua.kh.loginov.testplayerapp.dagger.modules.DownloaderModule
import ua.kh.loginov.testplayerapp.dagger.modules.MyDatabaseModule
import ua.kh.loginov.testplayerapp.data.Downloader
import ua.kh.loginov.testplayerapp.model.VideoItemDao
import javax.inject.Singleton

@Singleton
@Component(modules = [MyDatabaseModule::class, DownloaderModule::class])
interface DataComponent {
    fun getVideoItemDao(): VideoItemDao
    fun getDownloader(): Downloader
}