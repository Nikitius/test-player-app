package ua.kh.loginov.testplayerapp.ui.adapters

import android.view.View
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ua.kh.loginov.testplayerapp.MyApplication
import ua.kh.loginov.testplayerapp.R
import ua.kh.loginov.testplayerapp.base.Observer
import ua.kh.loginov.testplayerapp.base.OnClickListener
import ua.kh.loginov.testplayerapp.databinding.VideoItemBinding
import ua.kh.loginov.testplayerapp.model.VideoItem

class VideoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var binding: VideoItemBinding? = DataBindingUtil.bind(itemView)

    init {
        binding?.onDownloadClickListener = object : OnClickListener {
            override fun onClick(v: View) {
                val video = binding!!.video!!
                if (VideoItem.Status.DOWNLOADED != video.status) {
                    val iv = v as ImageView
                    video.status = VideoItem.Status.DOWNLOADING
                    iv.isClickable = false
                    iv.setImageResource(R.drawable.ic_downloading_24)

                    val app = binding!!.root.context.applicationContext as MyApplication
                    app.downloader.downloadForVideoItem(
                        video,
                        object : Observer<Any> {
                            override fun onNext(t: Any?) {
                                iv.post {
                                    iv.isClickable = true
                                    iv.setImageResource(R.drawable.ic_download_24)
                                }
                            }
                        },
                        object : Observer<Any> {
                            override fun onNext(t: Any?) {
                                iv.post {
                                    iv.setImageResource(R.drawable.ic_downloaded_24)
                                }
                            }
                        }
                    )
                }
            }
        }
    }

}