package ua.kh.loginov.testplayerapp.model

import androidx.room.*

@Dao
interface VideoItemDao {

    @Query("SELECT * FROM videos ORDER BY status")
    fun getAll(): List<VideoItem>

    @Query("SELECT * FROM videos WHERE status = :status")
    @TypeConverters(RoomConverter::class)
    fun  getAllForStatus(status: VideoItem.Status): List<VideoItem>

    @Query("SELECT count(*) FROM videos WHERE status = :status")
    @TypeConverters(RoomConverter::class)
    fun  getAmountForStatus(status: VideoItem.Status): Int

    @Query("SELECT status FROM videos WHERE id = :id")
    @TypeConverters(RoomConverter::class)
    fun getStatusForId(id: String): VideoItem.Status

    @Query("DELETE FROM videos WHERE status = :status")
    @TypeConverters(RoomConverter::class)
    fun  deleteAllForStatus(status: VideoItem.Status)

    @Insert
    fun insert(videoItem: VideoItem)

    @Update
    fun update(videoItem: VideoItem)
}