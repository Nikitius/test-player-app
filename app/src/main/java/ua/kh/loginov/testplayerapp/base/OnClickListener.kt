package ua.kh.loginov.testplayerapp.base

import android.view.View

interface OnClickListener {
    fun onClick(v: View)
}