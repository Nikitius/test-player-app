package ua.kh.loginov.testplayerapp.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.*

@Entity(tableName = "videos")
@TypeConverters(RoomConverter::class)
data class VideoItem constructor(
    @PrimaryKey val id: String,
    val  title: String,
    @ColumnInfo(name = "image_url") var imageURL: String,
    @ColumnInfo(name = "video_uri") var videoURI: String,
    @ColumnInfo(name = "audio_uri") var audioURI: String,
    @TypeConverters(RoomConverter::class) var status: Status
) : Parcelable {

    companion object {
        @Ignore
        @JvmField
        val CREATOR: Parcelable.Creator<VideoItem> = object : Parcelable.Creator<VideoItem> {
            override fun createFromParcel(source: Parcel?): VideoItem {
                return VideoItem(source)
            }

            override fun newArray(size: Int): Array<VideoItem?> {
                return Array(size) {null}
            }

        }
    }

    private constructor(source: Parcel?) : this(
        source!!.readString() as String,
        source.readString() as String,
        source.readString() as String,
        source.readString() as String,
        source.readString() as String,
        Status.fromString(source.readString()) as Status
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        if (dest !== null) {
            dest.writeString(id)
            dest.writeString(title)
            dest.writeString(imageURL)
            dest.writeString(videoURI)
            dest.writeString(audioURI)
            dest.writeString(status.name)
        }
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        if (other === null)
            return false
        if (this === other)
            return true
        if (other !is VideoItem)
            return false
        return id == other.id
    }

    @TypeConverters(RoomConverter::class)
    enum class Status {
        CACHED, DOWNLOADING, DOWNLOADED;

        companion object {
            fun fromString(s: String?): Status? {
                for (value in values())
                    if (value.name == s)
                        return value
                return null
            }
        }
    }

}

