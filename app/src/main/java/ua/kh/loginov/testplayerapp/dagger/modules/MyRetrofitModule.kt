package ua.kh.loginov.testplayerapp.dagger.modules

import dagger.Module
import dagger.Provides
import ua.kh.loginov.testplayerapp.retrofit.MyRetrofit
import javax.inject.Singleton

@Module
class MyRetrofitModule {
    @Provides
    @Singleton
    fun provideMyRetrofit(): MyRetrofit {
        return MyRetrofit()
    }
}