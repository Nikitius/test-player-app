package ua.kh.loginov.testplayerapp.model

import androidx.room.TypeConverter

class RoomConverter {

    @TypeConverter
    fun statusToString(value: VideoItem.Status): String {
        return value.name
    }

    @TypeConverter
    fun stringToStatus(value: String): VideoItem.Status {
        if (VideoItem.Status.CACHED.name == value)
            return VideoItem.Status.CACHED
        if (VideoItem.Status.DOWNLOADED.name == value)
            return VideoItem.Status.DOWNLOADED
        throw IllegalArgumentException("String value $value can`t be converted to enum type Status")
    }
}