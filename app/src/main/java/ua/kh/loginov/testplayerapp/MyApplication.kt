package ua.kh.loginov.testplayerapp

import android.app.Application
import ua.kh.loginov.testplayerapp.dagger.components.DaggerDataComponent
import ua.kh.loginov.testplayerapp.dagger.components.DaggerMyRetrofitComponent
import ua.kh.loginov.testplayerapp.dagger.modules.DownloaderModule

import ua.kh.loginov.testplayerapp.dagger.modules.MyDatabaseModule
import ua.kh.loginov.testplayerapp.dagger.modules.MyRetrofitModule
import ua.kh.loginov.testplayerapp.data.Downloader
import ua.kh.loginov.testplayerapp.model.VideoItemDao
import ua.kh.loginov.testplayerapp.retrofit.MyRetrofit

class MyApplication : Application() {
    lateinit var myRetrofit: MyRetrofit
    lateinit var videoItemDao: VideoItemDao
    lateinit var downloader: Downloader

    override fun onCreate() {
        super.onCreate()
        myRetrofit = DaggerMyRetrofitComponent
            .builder()
            .myRetrofitModule(MyRetrofitModule())
            .build()
            .getMyRetrofit()

        val daggerDataComponent = DaggerDataComponent
            .builder()
            .myDatabaseModule(MyDatabaseModule(this))
            .downloaderModule(DownloaderModule(this))
            .build()
        videoItemDao = daggerDataComponent.getVideoItemDao()
        downloader = daggerDataComponent.getDownloader()
    }
}