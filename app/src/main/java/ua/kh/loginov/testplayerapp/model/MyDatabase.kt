package ua.kh.loginov.testplayerapp.model

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [VideoItem::class], version = 1)
abstract class MyDatabase : RoomDatabase() {
    abstract fun videoItemDao(): VideoItemDao
}