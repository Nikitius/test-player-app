package ua.kh.loginov.testplayerapp.retrofit.model

import com.google.gson.annotations.SerializedName

data class Root (
    @SerializedName("items") val items : List<Item>
)