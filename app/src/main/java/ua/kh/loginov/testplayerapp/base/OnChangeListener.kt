package ua.kh.loginov.testplayerapp.base

abstract class OnChangeListener<T>(
    var `object`: T
) {
    abstract fun onChange()
}