package ua.kh.loginov.testplayerapp.dagger.components

import dagger.Component
import ua.kh.loginov.testplayerapp.dagger.modules.MyRetrofitModule
import ua.kh.loginov.testplayerapp.retrofit.MyRetrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [MyRetrofitModule::class])
interface MyRetrofitComponent {
    fun getMyRetrofit(): MyRetrofit
}