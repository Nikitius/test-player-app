package ua.kh.loginov.testplayerapp.dagger.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import ua.kh.loginov.testplayerapp.data.Downloader
import ua.kh.loginov.testplayerapp.model.VideoItemDao
import javax.inject.Singleton

@Module
class DownloaderModule(private val context: Context) {
    @Provides
    @Singleton
    fun provideDownloader(videoItemDao: VideoItemDao): Downloader {
        return Downloader(context, videoItemDao)
    }
}