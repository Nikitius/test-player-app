package ua.kh.loginov.testplayerapp.ui.activities.player

import android.os.Bundle
import ua.kh.loginov.testplayerapp.BR
import ua.kh.loginov.testplayerapp.R
import ua.kh.loginov.testplayerapp.base.BaseActivity
import ua.kh.loginov.testplayerapp.databinding.ActivityPlayerBinding
import ua.kh.loginov.testplayerapp.model.VideoItem

class PlayerActivity : BaseActivity<ActivityPlayerBinding, PlayerActivityViewModel>() {

    companion object {
        private const val EXTRAS_VIDEOS = "videos"
        private const val EXTRAS_POSITION = "position"
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        viewModel.onRestoreInstanceState(savedInstanceState)
    }

    override fun onPause() {
        super.onPause()
        viewModel.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putAll(viewModel.onSaveInstanceState())
    }

    override fun createViewModel(): PlayerActivityViewModel {
        val intent = intent
        return PlayerActivityViewModel(
            this,
            intent.getParcelableArrayListExtra<VideoItem>(EXTRAS_VIDEOS) as List<VideoItem>,
            intent.getIntExtra(EXTRAS_POSITION, -1)
        )
    }

    override fun getLayout(): Int {
        return R.layout.activity_player
    }

    override fun getVariable(): Int {
        return BR.viewModel
    }

}