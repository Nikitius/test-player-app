package ua.kh.loginov.testplayerapp.base

import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable

interface Observer<T> : Observer<T> {
    override fun onComplete() {}
    override fun onSubscribe(d: Disposable?) {}
    override fun onError(e: Throwable?) {
        throw e!!
    }
}