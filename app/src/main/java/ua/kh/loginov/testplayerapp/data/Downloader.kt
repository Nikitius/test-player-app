package ua.kh.loginov.testplayerapp.data

import android.content.Context
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.Func
import io.reactivex.rxjava3.core.Observable
import ua.kh.loginov.testplayerapp.base.FetchListener
import ua.kh.loginov.testplayerapp.base.Observer
import ua.kh.loginov.testplayerapp.model.VideoItem
import ua.kh.loginov.testplayerapp.model.VideoItemDao

class Downloader(
    private val context: Context,
    private val videoItemDao: VideoItemDao
) {

    companion object {
        private const val VIDEO_ITEM = 0
        private const val ON_CANCEL_OBSERVER = 1
        private const val ON_COMPLETE_OBSERVER = 2

        private const val IMAGE_GROUP = 10
        private const val VIDEO_GROUP = 11
        private const val AUDIO_GROUP = 12
    }

    private val fetch: Fetch
    private val downloadData = HashMap<String, HashMap<Int, Any>>()

    init {
        val fetchConfiguration = FetchConfiguration.Builder(context)
            .setDownloadConcurrentLimit(3)
            .build()
        fetch = Fetch.Impl.getInstance(fetchConfiguration)
        fetch.addListener(object : FetchListener {
            private val downloadsForId = HashMap<String, Int>(15)

            override fun onError(download: Download, error: Error, throwable: Throwable?) {
                super.onError(download, error, throwable)
                val id = download.tag!!
                synchronized(downloadsForId) {
                    if (downloadsForId.containsKey(id))
                        downloadsForId.remove(id)
                    if (downloadData.containsKey(id))
                        removeDownloadDataSet(id)
                }
                Observable.just(Any()).subscribe(getOnCancelObserver(id))
            }

            override fun onCancelled(download: Download) {
                super.onCancelled(download)
                val id = download.tag!!
                synchronized(downloadsForId) {
                    if (downloadsForId.containsKey(id))
                        downloadsForId.remove(id)
                    if (downloadData.containsKey(id))
                        removeDownloadDataSet(id)
                }
            }

            override fun onCompleted(download: Download) {
                super.onCompleted(download)
                val id = download.tag!!
                synchronized(downloadsForId) {
                    if (!downloadsForId.containsKey(id))
                        downloadsForId[id] = 3

                    downloadsForId[id] = downloadsForId[id]!! - 1
                    val videoItem = getVideoItem(id)
                    when(download.group) {
                        IMAGE_GROUP -> videoItem.imageURL = download.file
                        VIDEO_GROUP -> videoItem.videoURI = download.file
                        AUDIO_GROUP -> videoItem.audioURI = download.file
                    }
                    if (downloadsForId[id] == 0) {
                        videoItem.status = VideoItem.Status.DOWNLOADED
                        Observable.just(Any()).subscribe(getOnCompleteObserver(id))
                        downloadsForId.remove(id)
                        downloadData.remove(id)
                        object : Thread() {
                            override fun run() {
                                super.run()
                                videoItemDao.update(videoItem)
                            }
                        }.start()
                    }
                }
            }
        })
    }

    fun downloadForVideoItem(
        videoItem: VideoItem,
        observerOnCancel: Observer<Any>,
        observerOnComplete: Observer<Any>
    ) {
        val downloadDataSet = HashMap<Int, Any>()
        val id = videoItem.id
        synchronized(downloadData) {
            downloadDataSet[VIDEO_ITEM] = videoItem
            downloadDataSet[ON_CANCEL_OBSERVER] = observerOnCancel
            downloadDataSet[ON_COMPLETE_OBSERVER] = observerOnComplete
            downloadData[id] = downloadDataSet
        }
        val filePath = context.filesDir.path
        val imagePath = "$filePath/$id-image"
        val videoPath = "$filePath/$id-video"
        val audioPath = "$filePath/$id-audio"
        val imageRequest = Request(videoItem.imageURL, imagePath)
        val videoRequest = Request(videoItem.videoURI, videoPath)
        val audioRequest = Request(videoItem.audioURI, audioPath)
        imageRequest.groupId = IMAGE_GROUP
        videoRequest.groupId = VIDEO_GROUP
        audioRequest.groupId = AUDIO_GROUP
        val requests = listOf(imageRequest, videoRequest, audioRequest)

        for (request in requests) {
            request.priority = Priority.HIGH
            request.networkType = NetworkType.ALL
            request.autoRetryMaxAttempts = 3
            request.tag = id
        }

        enqueueRequests(requests, 0)
    }

    private fun enqueueRequests(requests: List<Request>, position: Int) {
        fetch.enqueue(
            requests[position],
            Func {
                if (position < requests.size - 1)
                    enqueueRequests(requests, position + 1)
            },
            Func {
                val id = requests[position].tag!!
                Observable.just(Any()).subscribe(getOnCancelObserver(id))
                removeDownloadDataSet(id)
                for (i in 0 until position)
                    fetch.cancel(requests[i].id)
            }
        )
    }

    @Synchronized
    private fun getVideoItem(id: String): VideoItem {
        return downloadData[id]!![VIDEO_ITEM] as VideoItem
    }

    @Suppress("UNCHECKED_CAST")
    @Synchronized
    private fun getOnCancelObserver(id: String): Observer<Any> {
        return downloadData[id]!![ON_CANCEL_OBSERVER] as Observer<Any>
    }

    @Suppress("UNCHECKED_CAST")
    @Synchronized
    private fun getOnCompleteObserver(id: String): Observer<Any> {
        return downloadData[id]!![ON_COMPLETE_OBSERVER] as Observer<Any>
    }

    @Synchronized
    private fun removeDownloadDataSet(id: String) {
        downloadData.remove(id)
    }
}