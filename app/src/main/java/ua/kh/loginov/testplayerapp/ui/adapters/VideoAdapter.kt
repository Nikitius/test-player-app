package ua.kh.loginov.testplayerapp.ui.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ua.kh.loginov.testplayerapp.base.OnClickListener
import ua.kh.loginov.testplayerapp.databinding.VideoItemBinding
import ua.kh.loginov.testplayerapp.model.VideoItem
import ua.kh.loginov.testplayerapp.ui.activities.player.PlayerActivity


class VideoAdapter : RecyclerView.Adapter<VideoViewHolder>() {

    companion object {
        private const val EXTRAS_VIDEOS = "videos"
        private const val EXTRAS_POSITION = "position"
    }

    private val items = ArrayList<VideoItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = VideoItemBinding.inflate(inflater, parent, false)
        return VideoViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        val binding: VideoItemBinding = holder.binding!!
        binding.video = items[position]
        binding.onPlayClickListener = object : OnClickListener {
            override fun onClick(v: View) {
                val context = holder.binding!!.root.context
                val intent = Intent(context, PlayerActivity::class.java)
                intent.putParcelableArrayListExtra(EXTRAS_VIDEOS, items)
                intent.putExtra(EXTRAS_POSITION, position)
                context.startActivity(intent)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setData(newData: List<VideoItem>) {
        clear()
        items.addAll(newData)
        notifyDataSetChanged()
    }

    fun addData(newData: List<VideoItem>) {
        val startingPosition = items.size
        items.addAll(newData)
        notifyItemRangeInserted(startingPosition, newData.size)
    }

    fun removeItem(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    fun removeItem(item: VideoItem) {
        removeItem(items.indexOf(item))
    }

    fun getDataList(): List<VideoItem> {
        return items
    }

    fun addItem(item: VideoItem) {
        items.add(item)
        notifyItemInserted(items.size - 1)
    }

    fun removeItems(items: List<VideoItem>) {
        for (item in items) {
            removeItem(item)
        }
    }

    fun clear() {
        val end = items.size - 1
        items.clear()
        notifyItemRangeRemoved(0, end)
    }
}
