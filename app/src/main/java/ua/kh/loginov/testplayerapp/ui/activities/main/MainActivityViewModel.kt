package ua.kh.loginov.testplayerapp.ui.activities.main

import android.view.View
import android.widget.ProgressBar
import ua.kh.loginov.testplayerapp.base.ActivityViewModel
import ua.kh.loginov.testplayerapp.base.Observer
import ua.kh.loginov.testplayerapp.base.OnChangeListener
import ua.kh.loginov.testplayerapp.model.VideoItem
import ua.kh.loginov.testplayerapp.ui.adapters.VideoAdapter
import ua.kh.loginov.testplayerapp.data.*
import ua.kh.loginov.testplayerapp.data.DataUtils

class MainActivityViewModel(
    val callback: MainActivity
) : ActivityViewModel(callback) {

    val adapter = VideoAdapter()
    val onChangeListener = object : OnChangeListener<ProgressBar?>(null) {

        override fun onChange() {
            if (adapter.getDataList().isNotEmpty())
                `object`?.visibility = View.GONE
            else
                `object`?.visibility = View.VISIBLE
        }
    }

    init {
        getData()
    }

    private fun getData() {
        val observer = object : Observer<List<VideoItem>> {
            override fun onNext(t: List<VideoItem>?) {
                callback.runOnUiThread {
                    adapter.setData(t as List<VideoItem>)
                    onChangeListener.onChange()
                }
            }
        }

        val app = callback.getMyApplication()
        DataProvider(app.myRetrofit, app.videoItemDao, observer).provide()
    }

    private fun clearCache() {
        val observer = object : Observer<List<VideoItem>> {
            override fun onNext(t: List<VideoItem>?) {
                callback.runOnUiThread {
                    adapter.setData(t!!)
                    onChangeListener.onChange()
                    getData()
                }
            }
        }

        DataUtils.clearCache(callback.getMyApplication().videoItemDao, observer)
    }

    fun onClearCacheButtonClick() {
        if (adapter.getDataList().isNotEmpty()) clearCache()
    }
}