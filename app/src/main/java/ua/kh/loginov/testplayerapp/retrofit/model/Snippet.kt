package ua.kh.loginov.testplayerapp.retrofit.model

import com.google.gson.annotations.SerializedName

data class Snippet (
    @SerializedName("title") val title : String,
    @SerializedName("thumbnails") val thumbnails : Thumbnails
)