package ua.kh.loginov.testplayerapp.ui.activities.main

import android.view.View
import ua.kh.loginov.testplayerapp.BR
import ua.kh.loginov.testplayerapp.R
import ua.kh.loginov.testplayerapp.base.BaseActivity
import ua.kh.loginov.testplayerapp.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding, MainActivityViewModel>() {

    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun createViewModel(): MainActivityViewModel {
        return MainActivityViewModel(this)
    }

    override fun getVariable(): Int {
        return BR.viewModel
    }

    fun onClearCacheButtonClick(view: View) {
        viewModel.onClearCacheButtonClick()
    }

}