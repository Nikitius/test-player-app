package ua.kh.loginov.testplayerapp.data

import com.github.kotvertolet.youtubejextractor.YoutubeJExtractor
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import ua.kh.loginov.testplayerapp.model.VideoItem
import ua.kh.loginov.testplayerapp.model.VideoItemDao
import ua.kh.loginov.testplayerapp.retrofit.model.Root

class DataUtils {
    companion object {
        private const val RETROFIT_MODEL_TO_APP_MODEL_VIDEO_QUALITY_LEVEL = "720p"
        private const val RETROFIT_MODEL_TO_APP_MODEL_VIDEO_EXTENSION = "mp4"
        private const val RETROFIT_MODEL_TO_APP_MODEL_AUDIO_EXTENSION = "mp4"

        @JvmStatic
        fun retrofitModelToAppModel(r: Root, maxAttempts: Int): List<VideoItem> {
            val videos = ArrayList<VideoItem>()

            val youtubeJExtractor = YoutubeJExtractor()

            for (item in r.items) {
                var isOk = false
                var attempts = 1
                while (!isOk && attempts <= maxAttempts) {
                    try {
                        var videoURI = ""
                        var audioURI = ""

                        val youtubeStreamingData = youtubeJExtractor.extract(item.id).streamingData
                        for (stream in youtubeStreamingData.adaptiveVideoStreams)
                            if (RETROFIT_MODEL_TO_APP_MODEL_VIDEO_QUALITY_LEVEL == stream.qualityLabel
                                && RETROFIT_MODEL_TO_APP_MODEL_VIDEO_EXTENSION == stream.extension
                            ) {

                                videoURI = stream.url
                                break
                            }
                        for (stream in youtubeStreamingData.adaptiveAudioStreams)
                            if (RETROFIT_MODEL_TO_APP_MODEL_AUDIO_EXTENSION == stream.extension) {
                                audioURI = stream.url
                                break
                            }

                        videos.add(
                            VideoItem(
                                item.id,
                                item.snippet.title,
                                item.snippet.thumbnails.default.url,
                                videoURI,
                                audioURI,
                                VideoItem.Status.CACHED
                            ))
                        isOk = true
                    } catch (e: Exception) {
                        e.printStackTrace()
                        attempts++
                    }
                }
            }

            return videos
        }

        @JvmStatic
        fun clearCache(videoItemDao: VideoItemDao, observer: Observer<List<VideoItem>>) {
            object : Thread() {
                override fun run() {
                    super.run()
                    videoItemDao.deleteAllForStatus(VideoItem.Status.CACHED)

                    Observable
                        .just(ArrayList<VideoItem>())
                        .subscribe(observer)
                }
            }.start()
        }

    }
}